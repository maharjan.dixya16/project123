/// <reference types='Cypress' />\
describe('Billing Test Suites', function () {
  let credentials, location, value;

  before(() => {
    cy.fixture('login').then((data) => {
      credentials = data;
    });
    cy.fixture('contacts').then((data) => {
      value = data;
    });
  
  });

  beforeEach(() => {
    cy.session('login', () => {
      cy.loginWithoutUI(credentials);
      cy.visit('/web#cids=1');
      setAllowedLocation(location.hospital);
      cy.reload();
    });
  });

  it('should add a contact',function(){
    cy.get('.nav-main').within(()=>{
      cy.get('.nav-main-item').eq(1).click();   
    });
    cy.get('[data-tracking-button="contacts-add-contact"]').click();
    cy.get('#JdD8HuQoKYSZg9xst').type(value.firstName);
    // cy.get('#RhL7rL8MQN6CBnsR7').type(value.middleName);
    cy.get('#QoFkrbBfdzZeLMsEy').type(value.lastName);
    cy.get('#4DNG6kun8wT87cSkQ').type(value.email);
    cy.get('#mcPANCMdkgSGRzCs2').select('Cure Center');
    cy.get('#B6Tamm9EZKReuJL38').type(value.companyRole);
    cy.get('#ghMPJNynRhoFRPsheday').select('1');
    cy.get('#6kA4f4bnB6D2MPwDFmonth').select('February');
    cy.get('#6kA4f4bnB6D2MPwDFyear').select('2023');
    cy.get('[data-tracking-button="add-person-contact-modal-save"]').click();
  });

  it('should edit contact',function(){
    cy.get('[data-tracking-table="contacts-Contacts-0-0"]').click();
    cy.get('.btn btn-dual').first().click();
    cy.get('.block mb-0').within(()=>{
      // cy.get('#9K2jJghdyXLGGEEZc').select('Mr.');
      cy.get('#MQ8mKR2wsJFDTguQw').type(value.middleName);
      cy.get('.btn btn-sm btn-primary').click();
    });
    cy.get('.undefined').contains(value.middleName);
  });

  it('should delete contact',function(){
    cy.get('.text-center td-selector').eq(0).click();
    cy.get('#j8BQuXKZF3orKWAtt').click();
    cy.get('.dropdown-item').eq(1).click();
  });
});