/// <reference types='Cypress' />\
describe('Billing Test Suites', function () {
  let credentials, campaign;

  before(() => {
    cy.fixture('login').then((data) => {
      credentials = data;
    });
    cy.fixture('campaign').then((data) => {
      campaign = data;
    });

  });

  beforeEach(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    cy.session('login', () => {
      cy.visit('dev.keela.co');
      cy.get('#user-email').type(credentials.email);
      cy.get('#user-password').type(credentials.password);
       cy.get('.btn-block').click();
    });
  });
  
  it.only('should add a new campaign', function () {
  
    // cy.visit('dev.keela.co');
    // cy.get('#user-email').type(credentials.email);
    // cy.get('#user-password').type(credentials.password);
    // cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Campaigns"]').click();
    });
    cy.get('[data-tracking-button="campaigns-add-campaign"]').click();
    cy.get('[data-tracking-input="campaigns-Campaigns-name"] input').type(campaign.campaignName);
    cy.get('[data-tracking-input="campaigns-Campaigns-description"] textarea').type(campaign.descriptionField);
    cy.get('[data-tracking-input="campaigns-Campaigns-fundraisingGoal"] input').type(campaign.goal);
    cy.get('[data-tracking-button="add-campaign-modal-save"]').click();
  });

  it('should edit a campaign', function () {
    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.wait(5000)
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Campaigns"]').click();
    });
    cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').click();
    cy.get('[data-tracking-button="campaign-overview-actions"]').click();
    cy.get('.dropdown-menu-right rounded').within(() => {

    });
    cy.get('.block mb-0').within(() => {
      cy.get('#JddXy5NkgotnTxu9L').clear().type(campaign.updateGoal);
      cy.get('[data-tracking-button="add-campaign-modal-save"]').click();
    });
    cy.get('.font-size-h5 font-w500').should('include.text', campaign.updateGoal);
  });

  it('should delete a campaign', function () {
    cy.get('.nav-main').within(() => {
      cy.get('[data-tracking-navigation="Campaigns"]').click();
    });
    cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').click();
    cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').invoke('text').then((text) => {
      console.log(text);


      cy.get('#DxprRNzJneBxS4CwY').click();
      cy.get('.dropdown-menu-right rounded').within(() => {

      });
      cy.get('.swal2-input swal2-inputerror').type(campaign.text);
      cy.get('.swal2-confirm swal2-styled').should('be.visible').click();
      cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').should('not.include.text', text);
    });
  });
});
